import csv
import json
import requests
from requests.structures import CaseInsensitiveDict

path ="/home/TnaAZ/stock_exchange_data_and_scripts/data/Euronext_Equities_2022-03-21.csv"

#Creating the elasticsearch index & type

request_headers = CaseInsensitiveDict()
request_headers["Content-Type"] = "application/json"
request_headers["Authorization"] = "Basic ZWxhc3RpYzprdi1KTWZWZ0dYdkR3eFc2Qzcqdg=="

response = requests.delete('http://127.0.0.1:9200/euronext_equities',headers=request_headers,verify=False)

index = { "mappings": {
                "properties" : {
                  "name" :   {"type":"text"},
                  "isin" :   {"type":"text"},                  
                  "symbol" : {"type":"text"},
                  "market" : {"type":"text"}
                }
            }
    }

index_json = json.dumps(index)
response = requests.put('http://127.0.0.1:9200/euronext_equities',headers=request_headers,data=index_json,verify=False)
print(response.content)

file = open(path,encoding="cp1252")
csvreader = csv.reader(file,delimiter=';')

csv_headers = []
csv_headers = next(csvreader)

#Insert data in elasticsearch
i=0
for row in csvreader:
    if(len(row)>1):
        i = i+1
        euronext_data = {"name":row[0],"isin":row[1],"symbol":row[2],"market":row[3]}
        euronext_json_data = json.dumps(euronext_data)
        url = 'http://127.0.0.1:9200/euronext_equities/_doc/'+str(i)
        response = requests.post(url,data=euronext_json_data,headers=request_headers,verify=False)
        print(response.content)
file.close()


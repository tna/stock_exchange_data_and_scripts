import csv
import json
import requests
from requests.structures import CaseInsensitiveDict

path ="/home/TnaAZ/stock_exchange_data_and_scripts/data/equity_issuers.csv"

#Creating the elasticsearch index & type
request_headers = CaseInsensitiveDict()
request_headers["Content-Type"] = "application/json"
request_headers["Authorization"] = "Basic ZWxhc3RpYzprdi1KTWZWZ0dYdkR3eFc2Qzcqdg=="


index = { "mappings": {
                "properties" : {
                  "company" :                               {"type":"text"},
                  "symbol" :                                {"type":"text"},
                  "valor_number" :                          {"type":"text"},
                  "country" :                               {"type":"text"},
                  "traded_currency" :                       {"type":"text"},
                  "trading_platform" :                      {"type":"text"},
                  "class_of_share" :                        {"type":"text"},
                  "nominal_value" :                         {"type":"text"},
                  "listing_segment" :                       {"type":"text"},
                  "accounting_rules" :                      {"type":"text"},
                  "next_general_meeting" :                  {"type":"text"},
                  "annual_closing_date" :                   {"type":"text"},
                  "auditors" :                              {"type":"text"},
                  "sustainability_report_opting_in" :       {"type":"text"},
                  "international_recognized_standard" :     {"type":"text"},
                  "primary_listing" :                       {"type":"text"},
                  "opting_clause" :                         {"type":"text"}
                }
            }
    }

index_json = json.dumps(index)
response = requests.put('http://127.0.0.1:9200/six',headers=request_headers,data=index_json,verify=False)
print(response.content)


file = open(path,encoding="cp1252")
csvreader = csv.reader(file,delimiter=';')

csv_headers = []
csv_headers = next(csvreader)

#Insert data in elasticsearch
i=0
for row in csvreader:
    if(len(row)>1):
        i = i+1
        six_data = {"company":row[0],"symbol":row[1],"valor_number":row[2],"country":row[3],"traded_currency":row[4],"trading_platform":row[5],"class_of_share":row[6],"nominal_value":row[7],"listing_segment":row[8],"accounting_rules":row[9],"next_general_meeting":row[10],"annual_closing_date":row[11],"auditors":row[12],"sustainability_report_opting_in":row[13],"international_recognized_standard":row[14],"primary_listing":row[15],"opting_clause":row[16]}
        six_json_data = json.dumps(six_data)
        url = 'http://127.0.0.1:9200/six/_doc/'+str(i)
        response = requests.post(url,data=six_json_data,headers=request_headers,verify=False)
        print(response.content)
file.close()

import csv
import json
import requests
from requests.structures import CaseInsensitiveDict

path ="/home/TnaAZ/stock_exchange_data_and_scripts/data/"

#Creating the elasticsearch index & type
request_headers = CaseInsensitiveDict()
request_headers["Content-Type"] = "application/json"
request_headers["Authorization"] = "Basic ZWxhc3RpYzprdi1KTWZWZ0dYdkR3eFc2Qzcqdg=="

response = requests.delete('http://127.0.0.1:9200/stock_exchange_data',headers=request_headers,verify=False)

index = { "mappings": {
                "properties" : {
                  "company_name" :  {"type":"text"},
                  "security" :      {"type":"text"},
                  "isin" :          {"type":"text"},
                  "symbol" :        {"type":"text"},
                  "cqs_symbol" :    {"type":"text"},
                  "nasdaq_symbol" : {"type":"text"},
                  "market" :        {"type":"text"},
                  "country" :       {"type":"text"},
                  "code" :          {"type":"text"},
                  "trading_name" :  {"type":"text"}
                }
            }
    }

index_json = json.dumps(index)
response = requests.put('http://127.0.0.1:9200/stock_exchange_data',headers=request_headers,data=index_json,verify=False)
print(response.content)

index = { "mappings": {
                "properties" : {
                  "future" :  {"type":"text"},
                  "code" :      {"type":"text"},
                  "market" :          {"type":"text"},
                  "country" :        {"type":"text"}
                }
            }
    }

index_json = json.dumps(index)
response = requests.put('http://127.0.0.1:9200/commodities_data',headers=request_headers,data=index_json,verify=False)
print(response.content)

#NYSE
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("NYSE")
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")

filename = "other-listed_csv.csv"
filepath = path + filename

file = open(filepath,encoding="cp1252")
csvreader = csv.reader(file,delimiter=',')

csv_headers = []
csv_headers = next(csvreader)

i=0
ci=0
for row in csvreader:
    if(len(row)>1):
        i = i+1
        nyse_data = {"symbol":row[0],"company_name":row[1],"security":row[2],"market":row[3],"cqs_symbol":row[4],"nasdaq_symbol":row[8]}
        print(nyse_data)
        nyse_json_data = json.dumps(nyse_data)
        url = 'http://127.0.0.1:9200/stock_exchange_data/_doc/'+str(i)
        response = requests.post(url,data=nyse_json_data,headers=request_headers,verify=False)
        print(response.content)
file.close()

#LSEG
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("LSEG")
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")

filename = "Issuer List Archive 2021.csv"
filepath = path + filename

file = open(filepath,encoding="cp1252")
csvreader = csv.reader(file,delimiter=',')

csv_headers = []
csv_headers = next(csvreader)

for row in csvreader:
    if(len(row)>1):
        if(row[3]!="Basic Resources"):
            i = i+1
            lseg_data = {"company_name":row[1],"country":row[4],"market":"LSE " + row[6]}
            print(lseg_data)
            lseg_json_data = json.dumps(lseg_data)
            url = 'http://127.0.0.1:9200/stock_exchange_data/_doc/'+str(i)
            response = requests.post(url,data=lseg_json_data,headers=request_headers,verify=False)
            print(response.content)
        else:
            ci = ci + 1
            lseg_data = {"future":row[1],"country":row[4],"market":"LSE " + row[6]}
            print(lseg_data)
            lseg_json_data = json.dumps(lseg_data)
            url = 'http://127.0.0.1:9200/commodities_data/_doc/'+str(ci)
            response = requests.post(url,data=lseg_json_data,headers=request_headers,verify=False)
            print(response.content)
file.close()

#SGX
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("SGX")
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")

filename = "SGX_securities.csv"
filepath = path + filename
file = open(filepath,encoding="cp1252")
csvreader = csv.reader(file,delimiter=';')

csv_headers = []
csv_headers = next(csvreader)

for row in csvreader:
    if(len(row)>1):
        i = i+1
        if(len(row) == 4):
            sgx_data = {"company_name":row[0],"isin":row[1],"code":row[2],"trading_name":row[3]}
        else:
            sgx_data = {"company_name":row[0],"isin":row[2],"code":row[3],"trading_name":row[4]}
        print(sgx_data)
        sgx_json_data = json.dumps(sgx_data)
        url = 'http://127.0.0.1:9200/stock_exchange_data/_doc/'+str(i)
        response = requests.post(url,data=sgx_json_data,headers=request_headers,verify=False)
        print(response.content)
file.close()

#SIX
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("SIX")
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")

filename = "equity_issuers.csv"
filepath = path + filename
file = open(filepath,encoding="cp1252")

csvreader = csv.reader(file,delimiter=';')

csv_headers = []
csv_headers = next(csvreader)

for row in csvreader:
    if(len(row)>1):
        i = i+1
        six_data = {"company_name":row[0],"symbol":row[1],"country":row[3],"market":row[5]}
        six_json_data = json.dumps(six_data)
        url = 'http://127.0.0.1:9200/stock_exchange_data/_doc/'+str(i)
        response = requests.post(url,data=six_json_data,headers=request_headers,verify=False)
        print(response.content)
file.close()

#ENX
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("ENX")
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")

filename = "Euronext_Equities_2022-03-21.csv"
filepath = path + filename
file = open(filepath,encoding="cp1252")

csvreader = csv.reader(file,delimiter=';')

csv_headers = []
csv_headers = next(csvreader)

for row in csvreader:
    if(len(row)>1):
        i = i+1
        euronext_data = {"company_name":row[0],"isin":row[1],"symbol":row[2],"market":row[3]}
        euronext_json_data = json.dumps(euronext_data)
        url = 'http://127.0.0.1:9200/stock_exchange_data/_doc/'+str(i)
        response = requests.post(url,data=euronext_json_data,headers=request_headers,verify=False)
        print(response.content)
file.close()

#ENX
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("ICE")
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")

filename = "commodities_data.txt"
filepath = path + filename
file = open(filepath,encoding="cp1252")

csvreader = csv.reader(file,delimiter=',')

csv_headers = []
csv_headers = next(csvreader)

for row in csvreader:
    if(len(row)>1):
        i = i+1
        ice_data = {"future":row[0],"code":row[1],"market":"ICE"}
        ice_json_data = json.dumps(ice_data)
        url = 'http://127.0.0.1:9200/commodities_data/_doc/'+str(ci)
        response = requests.post(url,data=ice_json_data,headers=request_headers,verify=False)
        print(response.content)
file.close()

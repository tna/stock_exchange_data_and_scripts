import csv
import json
import requests
from requests.structures import CaseInsensitiveDict

path ="/home/TnaAZ/stock_exchange_data_and_scripts/data/other-listed_csv.csv"

#Creating the elasticsearch index & type
request_headers = CaseInsensitiveDict()
request_headers["Content-Type"] = "application/json"
request_headers["Authorization"] = "Basic ZWxhc3RpYzprdi1KTWZWZ0dYdkR3eFc2Qzcqdg=="

response = requests.delete('http://127.0.0.1:9200/nyse',headers=request_headers,verify=False)

index = { "mappings": {
                "properties" : {
                  "act_symbol" :        {"type":"text"},
                  "company_name" :      {"type":"text"},
                  "security_name" :     {"type":"text"},
                  "exchange" :          {"type":"text"},
                  "cqs_symbol" :        {"type":"text"},
                  "etf" :               {"type":"text"},
                  "round_lot_size" :    {"type":"text"},
                  "test_issue" :        {"type":"text"},
                  "nasdaq_symbol" :     {"type":"text"}
                }
            }
    }

index_json = json.dumps(index)
response = requests.put('http://127.0.0.1:9200/nyse',headers=request_headers,data=index_json,verify=False)
print(response.content)


file = open(path,encoding="cp1252")
csvreader = csv.reader(file,delimiter=',')

csv_headers = []
csv_headers = next(csvreader)

#Insert data in elasticsearch
i=0
for row in csvreader:
    if(len(row)>1):
        i = i+1
        nyse_data = {"act_symbol":row[0],"company_name":row[1],"security_name":row[2],"exchange":row[3],"cqs_symbol":row[4],"etf":row[5],"round_lot_size":row[6],"test_issue":row[7],"nasdaq_symbol":row[8]}
        print(nyse_data)
        nyse_json_data = json.dumps(nyse_data)
        url = 'http://127.0.0.1:9200/nyse/_doc/'+str(i)
        response = requests.post(url,data=nyse_json_data,headers=request_headers,verify=False)
        print(response.content)
file.close()

import csv
import json
import requests
from requests.structures import CaseInsensitiveDict

path ="/home/TnaAZ/stock_exchange_data_and_scripts/data/SGX_securities.csv"

#Creating the elasticsearch index & type
request_headers = CaseInsensitiveDict()
request_headers["Content-Type"] = "application/json"
request_headers["Authorization"] = "Basic ZWxhc3RpYzprdi1KTWZWZ0dYdkR3eFc2Qzcqdg=="


index = { "mappings": {
                "properties" : {
                  "name" :                  {"type":"text"},
                  "status" :                {"type":"text"},
                  "isin" :                  {"type":"text"},
                  "code" :                  {"type":"text"},
                  "trading_counter_name" :  {"type":"text"}
                }
            }
    }

index_json = json.dumps(index)
response = requests.put('http://127.0.0.1:9200/sgx',headers=request_headers,data=index_json,verify=False)
print(response.content)


file = open(path,encoding="cp1252")
csvreader = csv.reader(file,delimiter=';')

csv_headers = []
csv_headers = next(csvreader)

#Insert data in elasticsearch
i=0
for row in csvreader:
    if(len(row)>1):
        i = i+1
        if(len(row) == 4):
            sgx_data = {"name":row[0],"status":None,"isin":row[1],"code":row[2],"trading_counter_name":row[3]}
        else:
            sgx_data = {"name":row[0],"status":row[1],"isin":row[2],"code":row[3],"trading_counter_name":row[4]}
        print(sgx_data)
        sgx_json_data = json.dumps(sgx_data)
        url = 'http://127.0.0.1:9200/sgx/_doc/'+str(i)
        response = requests.post(url,data=sgx_json_data,headers=request_headers,verify=False)
        print(response.content)
file.close()

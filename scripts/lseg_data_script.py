import csv
import json
import requests
from requests.structures import CaseInsensitiveDict

path ="/home/TnaAZ/stock_exchange_data_and_scripts/data/Issuer List Archive 2021.csv"

#Creating the elasticsearch index & type
request_headers = CaseInsensitiveDict()
request_headers["Content-Type"] = "application/json"
request_headers["Authorization"] = "Basic ZWxhc3RpYzprdi1KTWZWZ0dYdkR3eFc2Qzcqdg=="


index = { "mappings": {
                "properties" : {
                  "admission_date" :            {"type":"text"},
                  "company_name" :              {"type":"text"},
                  "icb_industry" :              {"type":"text"},
                  "icb_super-sector" :          {"type":"text"},
                  "country_of_incorporation" :  {"type":"text"},
                  "world_region" :              {"type":"text"},
                  "market" :                    {"type":"text"},
                  "international_issuer" :      {"type":"text"},
                  "company_market_cap" :        {"type":"text"}
                }
            }
    }

index_json = json.dumps(index)
response = requests.put('http://127.0.0.1:9200/lseg',headers=request_headers,data=index_json,verify=False)
print(response.content)


file = open(path,encoding="cp1252")
csvreader = csv.reader(file,delimiter=',')

csv_headers = []
csv_headers = next(csvreader)

#Insert data in elasticsearch
i=0
for row in csvreader:
    if(len(row)>1):
        i = i+1
        lseg_data = {"admission_date":row[0],"company_name":row[1],"icb_industry":row[2],"icb_super-sector":row[3],"country_of_incorporation":row[4],"world_region":row[5],"market":row[6],"international_issuer":row[7],"company_market_cap":row[8]}
        print(lseg_data)
        lseg_json_data = json.dumps(lseg_data)
        url = 'http://127.0.0.1:9200/lseg/_doc/'+str(i)
        response = requests.post(url,data=lseg_json_data,headers=request_headers,verify=False)
        print(response.content)
file.close()

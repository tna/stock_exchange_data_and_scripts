Futures,Code
"ICE Brent",B
"ICE WTI",T
"Murban Crude",ADM
"LS Gasoil",G
"UK Nat Gas",M
"ICE EUA",C
"ICE Henry",H
"TTF Gas",TFM
"Canola",RS
"Cocoa",CC
"London Cocoa",C
"Coffee "C"",KC
"Robusta Coffee",R
"Cotton No.2",CT
"Sugar No.11",SB
"White Sugar",W
"Mini-Gold Future",YG
"Mini-Silver Future",YI
"100 oz Gold Future",ZG
"5000 oz Silver Future",ZI